module.exports = {
    app: {
        files: ['dist/app.js'],
        tasks: [],
        options: {
            spawn: false,
            livereload: true,
        },
    },
    html: {
        files: ['src/**/*.html'],
        tasks: ['copy:html'],
        options: {
            livereload: true,
        }
    },
    css: {
        files: ['src/stylesheets/*.less'],
        tasks: ['less'],
        options: {
            livereload: true,
        }
    }
}
