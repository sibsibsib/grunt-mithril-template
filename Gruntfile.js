module.exports = function(grunt) {

    require('load-grunt-config')(grunt, {});

    grunt.registerTask(
        'build',
        [
            'less',
            'browserify',
            'uglify',
            'copy',
        ]
    );
    grunt.registerTask(
        'dev',
        [
            'copy:html',
            'less',
            'connect',
            'browserify',
            'watch',
        ]
    )
};
