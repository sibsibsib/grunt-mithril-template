module.exports = {
    options: {
        browserifyOptions: {
            debug: true,
            fullPaths: false,
        },
        watch: true,
        plugin: ['bundle-collapser/plugin']
    },
    build: {
        options: {
            browserifyOptions: {
                standalone: 'app',
                debug: true,
                fullPaths: false,
            },
            transform: [
                'mithrilify',
                ['babelify', {
                    "loose": 'all',
                    "stage": 0
                }],
                'aliasify',
            ],
            external: [
                'mithril',
            ]
        },
        files: {
            'dist/app.js': ['src/main.js']
        },
    },
    lib: {
        src: ['.'],
        dest: 'dist/lib.js',
        options: {
            alias: [
                'mithril:',
            ]
        }
    }

}
