module.exports = {
    styles: {
        options: {
            paths: ['./src/stylesheets'],
            compress: true,
            sourceMap: true,
            sourceMapURL: '/stylesheets/style.css.map',
            sourceMapBasepath: './src/',
            sourceMapRootpath: '/',
        },
        files: {
            './dist/stylesheets/style.css': './src/stylesheets/style.less'
        }
    }
}
